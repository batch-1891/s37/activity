const mongoose = require('mongoose')

const courseSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	passwoord: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: String,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile No. is required"]
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "UserID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String
				default: new Enrolled
			}
		}
	]
})

module.exports = mongoose.model('User', courseSchema)